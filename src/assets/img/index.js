import ImgLogo from './LogoPC.png'
import ImgBg   from './bg.jpg'
import ImgQRCode from './QRCode.png'
import ImgUSer from './favicon.png'

export {ImgLogo,ImgBg,ImgQRCode,ImgUSer}