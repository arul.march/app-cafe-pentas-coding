import React from 'react'
import { ImageBackground, StyleSheet, Text, View, Image } from 'react-native'
import { ImgBg, ImgLogo} from '../../assets'
import { Button, Gap} from '../../components'

const GetStarted = ({navigation}) => {
    return (
        <ImageBackground source={ImgBg} style={styles.page}>
            <View style={styles.wrap}>
                <View>
                    <Image source={ImgLogo} style={{ width:'100%', resizeMode:'contain' }} />
                    <Gap height={20}/>
                    <Text style={styles.title}>Dapatkan potongan harga dan promo menarik lainnya</Text>
                </View>
                <View>
                    <Button title="Get Started" colbtn='blue' coltext="white" onPress={()=> navigation.navigate('Register')}  />
                    <Gap height={20}/>
                    <Button type="secondary" colbtn='white' title="Sign In" onPress={()=> navigation.navigate('Login')}/>
                </View>
            </View>
        </ImageBackground>
    )
}

export default GetStarted

const styles = StyleSheet.create({
    wrap:{
        backgroundColor:'rgba(0,0,0,0.80)',
        flex:1,
        justifyContent:"space-between",
        padding:40,
     


    },
    page:{
        // padding:40,
        flex:1,
        // justifyContent   :"space-between",
        backgroundColor:'white',
    },
    title:{
        fontSize:35,
        fontFamily:'Nunito-SemiBold',
        color:'white',
        marginTop:91,
        color:'#FF8400'
    }
})
