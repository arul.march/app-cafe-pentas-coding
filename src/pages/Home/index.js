import React from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity, ImageBackground} from 'react-native'
import { Gap } from '../../components'
import {ImgLogo,ImgUSer} from '../../assets'


const Home = ({navigation}) => {
    return (
        <TouchableOpacity style={styles.page} onPress={()=> navigation.navigate('QRCode')}  >
            <View style={styles.card}>
                <View style={styles.headercard1}>
                        <Image source={ImgLogo} style={{ width:'45%', resizeMode:'contain' }} />
                </View> 
                <View style={styles.headercard2}>
                        <Text style={styles.title}>MEMBER CARD</Text>
                </View>
                <View style={styles.detail}>
                    <Text style={styles.name}>User Name</Text>
                    <Gap height={5} />
                    <Text style={styles.number}>7541 12456 48245 1264</Text>
                    <Gap height={5} />
                    <Text style={styles.address}>Bantargebang - Bekasi</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default Home

const styles = StyleSheet.create({
    card:{
        width:'100%',
        height:200,
        backgroundColor:'darkgrey',
        borderRadius:30,
    },
    page:{
        padding:20,
        justifyContent:'space-around',
        alignItems:'center',
        paddingVertical:20
    },
    title:{
        fontSize:20,
        fontFamily:'Nunito-Black',
        color:'orange',

    },

    // HEADER CARD 
    headercard1:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        paddingTop:10,
        paddingRight:25,
        alignItems:'center',
        // backgroundColor:'white',
    },
    headercard2:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        paddingRight:30,
        marginTop:-20,
        alignItems:'center',
        // backgroundColor:'white',
    },
    // END HEADER CARD 

    detail:{
        flex:3,
        justifyContent:'flex-end',
        padding: 20,
    },
    name:{
        fontSize:17,
        fontFamily:'Nunito-Bold',
        color:'black',
        textTransform:'uppercase'

    },
    number:{
        fontSize:17,
        fontFamily:'Nunito-Bold',
        color:'black',
    },
    address:{
        fontSize:14,
        fontFamily:'Nunito-SemiBold',
        color:'black'
    },
})
