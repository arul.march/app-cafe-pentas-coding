import React from 'react'
import { StyleSheet, View, Image,Text } from 'react-native'
import { ImgLogo } from '../../assets'
import { Button, Gap, Input } from '../../components'

const Login = ({navigation}) => {
    return (
       <View style={styles.page}>
            <View style={ styles.container}>
                    <Image source={ImgLogo} style={{ width:'100%', resizeMode:'contain' }} />
                     <Text style={styles.title} >Web & Mobile App Development</Text>
                    <Gap height={60} />
                    <Input label="NIK :" />
                    <Gap height={25} />
                    <Input label="Password :" />
                    <Gap height={60} />
                    <Button title="Login" coltext="white" colbtn='orange' onPress={()=> navigation.replace('Home')} />
            </View>
       </View>
    )
}



export default Login

const styles = StyleSheet.create({
    page:{
        flex:1,
        padding:50,
        backgroundColor:'white',
        justifyContent:'center'
        
    
    },
    container:{
        // backgroundColor:'white',
    },
    title:{
        fontSize:17,
        marginTop:-25,
        fontFamily:'Nunito-SemiBold',
        fontStyle:'italic',
        textAlign:'center',
        color:'#7D8797',

    },
    title2:{
        fontSize:20,
        fontFamily:'Nunito-Bold',
        color:'#7D8797',

    }
})
