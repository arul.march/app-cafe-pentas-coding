import React from 'react'
import { Image, StyleSheet, View, Text,TouchableOpacity} from 'react-native'
import {ImgQRCode, ImgUSer} from '../../assets'
import {Gap} from '../../components'


const QRCode = ({navigation}) => {
    return (

       <View style={styles.page} >
            <View style={styles.box}>
                    <View style={styles.container}>
                        <View style={styles.imgwrap}>
                            <Image style={styles.imguser} source={ImgUSer} />
                        </View>
                        <Text style={styles.title}>User Name</Text>
                    </View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Home')}>
                        <Image style={styles.imgqrcode} source={ImgQRCode} />
                    </TouchableOpacity>
            </View>

       </View>
    )
}

export default QRCode

const styles = StyleSheet.create({
    
    page:{
        flex:1,
        backgroundColor:'lightgrey',
        alignItems:'center',
        justifyContent:'center',

    },
    box:{
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center',
        paddingRight:50,
        paddingLeft:50,
        paddingBottom:50,
        paddingTop:20,
        borderRadius:10
       
    },
    container:{
        alignItems:'center',
        top:-70
    },
    imguser:{
        width:80,
        height:80,
        borderRadius:40,

    },
    imgwrap:{
        width:100,
        height:100,
        borderRadius:50,
        backgroundColor:'white',
        alignItems:'center',
        justifyContent:'center',
    },
    imgqrcode:{
        width:200,
        height:200,
        resizeMode:'contain',
        top:-30
    },
    title:{
        fontFamily:'Nunito-black',
        fontSize:20
    }
})
