import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button, Gap, Input } from '../../components'

const Register = ({navigation}) => {
    return (
            <View style={styles.page}>
                <View>
                    <Input label="Full Name :" />
                    <Gap height={30} />
                    <Input label="Alamat :" />
                    <Gap height={30} />
                    <Input label="Email :" />
                    <Gap height={30} />
                    <Input label="Password :" />
                </View>
                <Button title="Continue" colbtn='orange' coltext='white' onPress={()=> navigation.replace('Home')} />
            </View>
    )
}

export default Register

const styles = StyleSheet.create({
    page:{
        padding:40,
        justifyContent:'space-evenly',
        backgroundColor:'white',
        flex:1
    }
})
