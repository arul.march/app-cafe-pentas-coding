import Splash from './Splash';
import Login from './Login';
import Home from './Home';
import GetStarted from './GetStarted';
import Register from './Register';
import QRCode from './QRCode';

export { Splash, Login, Home, GetStarted, Register, QRCode }